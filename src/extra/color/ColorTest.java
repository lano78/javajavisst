package extra.color;
/**
 * @author Lars Nordstr�m
 * @website http://www.lenbitz.se
 * @repo lano78@bitbucket.org
 * @twitter lano78@twitter
 * @date 6 aug. 2015
 * @license May not be redistributed or used without the consent of the author.
 */
import java.awt.Color;

public class ColorTest {

    public ColorTest() {
	
	System.out.println("Hex : " + HexColor.toHexFromRGB(255, 255, 255));
	
	System.out.println(Color.decode("0xffffff"));
    }

    public static void main(String[] args) {

	new ColorTest();
    }

}
