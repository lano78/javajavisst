package extra.color;
/**
 * @author Lars Nordstr�m
 * @website http://www.lenbitz.se
 * @repo lano78@bitbucket.org
 * @twitter lano78@twitter
 * @date 6 aug. 2015
 * @license May not be redistributed or used without the consent of the author.
 */
import java.awt.Color;

public class HexColor {
    
    /**
     * Static methods, constructor is private.
     */
    private HexColor(){}

    /**
     * Converts an RGB color value to Hex color value
     * @param r
     * @param g
     * @param b
     * @return hexvalue
     */
    public static String toHexFromRGB(int r, int g, int b) {
	return String.format("#%02x%02x%02x", r, g, b);
    }
    
    /**
     * Converts an RGBA color value to Hex color value
     * @param r
     * @param g
     * @param b
     * @param a
     * @return hexvalue
     */
    public static String toHexFromRGB(int r, int g, int b, int a) {
	return String.format("#%02x%02x%02x%02x", r, g, b, a);
    }

}
