package extra;
/**
 * @author Lars Nordstr�m
 * @website http://www.lenbitz.se
 * @repo lano78@bitbucket.org
 * @twitter lano78@twitter
 * @date 6 aug. 2015
 * @license May not be redistributed or used without the consent of the author.
 */
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.regex.Pattern;

/**
 * Class for searching for characters in strings.
 * 
 * @author lano78
 * @version 1.0
 */
public class Search {

    // The vowels.
    String[] vowels = { "a", "e", "i", "o", "u", "y", "�", "�", "�" };
    // The consonants.
    String[] consonants = { "b", "c", "d", "f", "g", "h", "j", "k", "l", "m", "n", "p", "q", "r", "s", "t", "v", "w",
	    "x", "z" };

    // Regex pattern.
    Pattern regex = Pattern.compile("", Pattern.UNICODE_CHARACTER_CLASS);

    /**
     * Default Constructor with no arguments.
     */
    public Search() {

    }

    /**
     * Returns true if the the character is a vowel.<br>
     * If the character provided is not a single character then the the search is terminated.
     * 
     * @param v
     *            as the character string to check
     * @return boolean
     */
    public boolean isVowel(String v) {

	for (String s : vowels) {

	    if (v.length() != 1)
		break;

	    if (s.equals(v.toLowerCase())) {
		System.out.println(" Found : " + s);
		return true;
	    }
	}
	return false;
    }

    /**
     * Returns true if the the character is a consonant.<br>
     * If the character provided is not a single character then the the search is terminated.
     * 
     * @param c
     *            as the character string to check
     * @return boolean
     */
    public boolean isConsonant(String c) {

	for (String s : consonants) {

	    if (c.length() != 1)
		break;

	    if (s.equals(c.toLowerCase())) {
		System.out.println(" Found : " + s);
		return true;
	    }
	}
	return false;
    }

    /**
     * Search a string for vowels and returns the count total.
     * 
     * @param s
     *            as in string to search
     * @return count as int
     */
    public int getVowelCount(String s) {
	int count = 0;
	String[] chars = new Split().splitStringToWordsArray(s, regex.pattern());
	while (true) {
	    for (String c : chars) {
		if (isVowel(c)) {
		    count++;
		}
	    }
	    break;
	}
	return count;
    }

    /**
     * Search a string for consonants and returns the count total.
     * 
     * @param s
     *            as in string to search
     * @return count as int
     */
    public int getConsonantCount(String s) {
	int count = 0;
	String[] chars = new Split().splitStringToWordsArray(s, regex.pattern());
	while (true) {
	    for (String c : chars) {
		if (isConsonant(c)) {
		    count++;
		}
	    }
	    break;
	}
	return count;
    }

    /**
     * Search a string and group letters and increment the count if there are many of same kind.
     * 
     * @param s
     *            as in the string to search.
     * @return ordered treeMap
     */
    public SortedMap<String, Integer> searchCharacters(String s) {

	SortedMap<String, Integer> sorted = new TreeMap<String, Integer>();

	String[] chars = new Split().splitStringToWordsArray(s, regex.pattern());

	while (true) {
	    for (String c : chars) {
		if (!sorted.containsKey(c)) {
		    sorted.put(c, 1);
		} else {
		    sorted.put(c, sorted.get(c) + 1);

		}
	    }
	    break;
	}

	System.out.println(sorted);
	return sorted;
    }
}
