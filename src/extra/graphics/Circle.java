package extra.graphics;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.RenderingHints;
import java.awt.geom.GeneralPath;
import javax.swing.JPanel;

/**
 * @author Lars Nordstr�m
 * @website http://www.lenbitz.se
 * @repo lano78@bitbucket.org
 * @twitter lano78@twitter
 * @date 5 aug. 2015
 * @license May not be redistributed or used without the consent of the author.
 */
public class Circle extends JPanel {

    // The points/degrees array
    private Point[] points;

    // Path f�r att rita linje.
    private GeneralPath path;

    // Tal j�mnt delbart med 360, l�gre heltal = j�mnare figur.
    private int smoothness = 0;

	private Color color = null;
    
    private boolean hasOutline = false;
    

    public Circle(double radius, double xPos, double yPos, int smoothness) {
	setSmoothness(smoothness);
	createPoints(radius, xPos, yPos);
	drawCircle();
    }
    
    public Circle(double radius, double xPos, double yPos, int smoothness, boolean hasOutline) {
	setHasOutline(hasOutline);
	setSmoothness(smoothness);
	createPoints(radius, xPos, yPos);
	drawCircle();
    }

    /**
     * 
     * @param radius
     * @param xPos
     * @param yPos
     */
    public Circle(double radius, double xPos, double yPos) {
	createPoints(radius, xPos, yPos);
	drawCircle();
    }

    /**
     * R�knar ut x/y f�r punkterna utifr�n radien.
     * 
     * @param radius
     * @param xPos
     * @param yPos
     */
    private void createPoints(double radius, double xPos, double yPos) {

	// Antalet punkter cirkeln skall inneh�lla.
	int numberOfPoints = 360 / getSmoothness();

	// initiera arrayen f�r punkterna.
	points = new Point[numberOfPoints];

	// Cirkelns x/y position och radie
	double cx = xPos;
	double cy = yPos;
	double r = radius;

	// index (theta)
	int idx = 0;

	// Loopar igenom punkterna
		int degreesForCircle = 360;
		for (int i = 0; i < degreesForCircle; i += getSmoothness()) {

	    // Skapar en punkt och sparar den i points array.
	    points[idx++] = new Point((int) (cx + r * Math.cos(Math.toRadians(i))),
		    (int) (cy + r * Math.sin(Math.toRadians(i))));

	}
    }

    /**
     * Ritar en cirkel utifr�n punkterna vi skapat
     */
    private void drawCircle() {

	path = new GeneralPath();

	// Index f�r att veta vilken punkt som ska anv�ndas.
	int idx = 0;

	// Ittererar �ver antalet points i arrayen
	for (Point point : points) {

	    // Kollar om det �r f�rsta punkten i arrayen.
	    if (idx == 0) {

		// Startpunkt f�r cirkeln
		path.moveTo(point.x, point.y);
	    } else {

		// N�sta punkt att rita ut.
		path.lineTo(point.x, point.y);
	    }
	    // Skriver ut punkterna i Console.
	    System.out.println(point.x + ", " + point.y);

	    // �kar indexet
	    idx++;
	}

	// St�nger path n�r vi �r klara.
	path.closePath();
    }

    @Override
    protected void paintComponent(Graphics graphics) {

	super.paintComponent(graphics);

	Graphics2D graphics2D = (Graphics2D) graphics;

	// Vi vill att cirkelns kant skall vara fin.
	graphics2D.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

	// H�mtar och s�tter fyllningsf�rgen
	graphics2D.setPaint(getColor());
	graphics2D.fill(path);

	// S�tter en gr�n outline(kant)
	graphics2D.setPaint(Color.green);
	
	// Ritar en outline f�r cirkeln
	createOutline(graphics2D);

    }

    private void createOutline(Graphics2D graphics2D) {
	// ritar ingen outline om hasOutline == false, kliver ut ut metoden.
	if (!hasOutline()) return;
	
	// Startpunkt f�r outline
	Point p1 = points[0];
	
	int idx = 1;
	
	// Ittererar �ver punkterna
	for (Point point : points) {
	    Point p2 = points[idx % points.length];
	    // Ritar en linje mellan p1 och p2
	    graphics2D.drawLine(p1.x, p1.y, p2.x, p2.y);
	    // byter sista punkt.
	    p1 = p2;
	    idx++;
	}
    }

    public int getSmoothness() {
	return (smoothness == 0) ? 1 : smoothness;
    }

    public void setSmoothness(int smoothness) {
	this.smoothness = smoothness;
    }

    public GeneralPath getPathForCircle() {
	return path;
    }

    public void setPathForCircle(GeneralPath path) {

	this.path = (this.path == null) ? new GeneralPath() : path;
    }

    public Point[] getPointsArray() {
	return points;
    }

    private Color getColor() {
	return (color == null) ? Color.RED : color;
    }

    public void setColor(Color color) {
	this.color = color;
    }
    public boolean hasOutline() {
        return hasOutline;
    }

    private void setHasOutline(boolean hasOutline) {
        this.hasOutline = hasOutline;
    }

}
