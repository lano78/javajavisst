package extra;
/**
 * @author Lars Nordstr�m
 * @website http://www.lenbitz.se
 * @repo lano78@bitbucket.org
 * @twitter lano78@twitter
 * @date 6 aug. 2015
 * @license May not be redistributed or used without the consent of the author.
 */
public class Benchmark {

    private Benchmark() {
    }

    /**
     * This method calculates the elapsed time between now and then.
     * Useful when benchmarking array itterations, animations, textureloading etc.
     * @param now
     *            current time in miliseconds.
     * @return diff (then - now)
     */
    public static long getElapsedTimeInMs(long now) {
	return (System.currentTimeMillis() - now);
    }
    
    public static long getElapsedTimeInSec(long now) {
	return (System.currentTimeMillis() - now)/1000;
    }
}
