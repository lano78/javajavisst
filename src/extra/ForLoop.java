package extra;
/**
 * @author Lars Nordstr�m
 * @website http://www.lenbitz.se
 * @repo lano78@bitbucket.org
 * @twitter lano78@twitter
 * @date 6 aug. 2015
 * @license May not be redistributed or used without the consent of the author.
 */
public class ForLoop {

    public static void main(String[] args) {
	for(int i = 0; i <= 20; i++){
	    
	    // Skriver ut efter en selektion.
	    if (i%2 == 0) System.out.println("J�mnt nummer : " + i);
	    
	    // Skriver ut enligt selektionsoperatorn
	    // System.out.println((i%2 == 0)? "J�mnt nummer : " + i : "");

	}

    }

}
