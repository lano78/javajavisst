package kap16;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.Scanner;
/**
 * @author Lars Nordstr�m
 * @website http://www.lenbitz.se
 * @repo lano78@bitbucket.org
 * @twitter lano78@twitter
 * @date 5 aug. 2015
 * @license May not be redistributed or used without the consent of the author.
 */
public class SkrivResultat {

    public static void main(String[] args) {

	Scanner scanner = new Scanner(System.in);

	String filnamn, poang, namn;

	System.out.print("Ange filnamnet : ");
	filnamn = scanner.nextLine();

	System.out.println("\nSkriv in namn och po�ng, avsluta med genom att skriva #");

	while(true){
	    try(PrintWriter write = new PrintWriter(new BufferedWriter(new FileWriter(filnamn, true)))){
		
		System.out.print("Ange ett namn : ");
		namn = scanner.nextLine();

		if(namn.equalsIgnoreCase("#")) 
		    break;

		System.out.print("\nAnge po�ng : ");
		poang = scanner.nextLine();
		
		
		write.println(namn);
		write.println(poang);
		

	    }catch(Exception ex){
		ex.printStackTrace();
	    }


	}
	
	System.out.println("Klart!");
	
	scanner.close();

    }

}
