package kap07;

import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

public class Betyg extends JFrame implements ActionListener {

    JLabel ledTxt, result;
    JButton btn;
    JTextField txtField;

    public Betyg() {

	this.setLayout(new FlowLayout());
	this.ledTxt = new JLabel("Skriv in po�ng");
	this.txtField = new JTextField(10);
	this.btn = new JButton("Tryck h�r");
	this.result = new JLabel();
	
	this.add(ledTxt);
	this.add(txtField);
	this.add(btn);
	this.add(result);
	
	this.txtField.addActionListener(this);
	this.btn.addActionListener(this);
    }

    @Override
    public void actionPerformed(ActionEvent arg0) {

	if (Integer.parseInt(txtField.getText()) >= 50) {
	    result.setForeground(new Color(0, 255, 0));
	    result.setText("Godk�nd");
	} else {
	    result.setForeground(new Color(255, 0, 0));
	    result.setText("Underk�nd");
	}
    }

    public static void main(String[] args) {
	Betyg betyg = new Betyg();
	betyg.setSize(400, 400);
	betyg.setLocationRelativeTo(null);
	betyg.setTitle("Betyg");
	betyg.setDefaultCloseOperation(EXIT_ON_CLOSE);
	betyg.setVisible(true);
    }

}
