package kap07;

import java.util.Scanner;

public class StringTest {

    public static void main(String[] args) {

	Scanner scanner = new Scanner(System.in);

	// Olika s�tt att j�mnf�ra str�ngar.
	System.out.println("Skriv ditt namn : ");
	String namn1 = scanner.nextLine();

	System.out.println("Skriv ditt namn igen eller ett annat : ");
	String namn2 = scanner.nextLine();

	// Kollar om orden �r exakt lika.
	if (namn1.equals(namn2)) {
	    System.out.println("Lika");
	} else {
	    System.out.println("Olika");
	}

	// Kollar om ordet �r lika, olika gemener och versaler spelar ingen roll.
	if (namn1.equalsIgnoreCase(namn2)) {
	    System.out.println("Lika");
	} else {
	    System.out.println("Olika");
	}

	scanner.close();
    }

}
