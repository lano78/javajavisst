package kap07;

import java.util.Scanner;

public class BoolTest {

    public static void main(String[] args) {
	
	Scanner scanner = new Scanner(System.in);
	
	System.out.print("Ange din �lder : ");
	int age = scanner.nextInt();
	
	boolean myndig = (age > 17) ? true : false;
	
	if(myndig){
	    System.out.println("Du �r myndig");
	}else{
	    System.out.println("Du �r inte myndig");
	}
	
	scanner.close();
    }

}
