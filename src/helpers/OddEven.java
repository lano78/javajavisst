package helpers;
/**
 * @author Lars Nordstr�m
 * @website http://www.lenbitz.se
 * @repo lano78@bitbucket.org
 * @twitter lano78@twitter
 * @date 6 aug. 2015
 * @license May not be redistributed or used without the consent of the author.
 */
public class OddEven {

    /**
     * Klassen kan inte instansieras pga metoderna �r statiska.
     */
    private OddEven() {
    }

    /**
     * Metod som kollar om ett heltal �r j�mnt eller oj�mnt.
     * Returen blir true om talet �r j�mnt, annars false.
     * @param i int
     * @return boolean
     */
    public static boolean isOddOrEven(int i) {
	return (i % 2 == 0) ? true : false;
    }
}
