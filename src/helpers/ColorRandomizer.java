package helpers;

import java.awt.Color;
/**
 * @author Lars Nordstr�m
 * @website http://www.lenbitz.se
 * @repo lano78@bitbucket.org
 * @twitter lano78@twitter
 * @date 6 aug. 2015
 * @license May not be redistributed or used without the consent of the author.
 */
public class ColorRandomizer {
    
    public static Color getRandomColor(){
	return new Color((int) (Math.random() * 255),(int) (Math.random() * 255),(int) (Math.random() * 255));
    }
}
