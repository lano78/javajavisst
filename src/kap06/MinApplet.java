/**
 * 
 */
package kap06;

import java.applet.Applet;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.HeadlessException;

/**
 * @author Lars Nordstr�m
 * @website http://www.lenbitz.se
 * @repo lano78@bitbucket.org
 * @twitter lano78@twitter
 * @date 1 july. 2015
 * @license May not be redistributed or used without the consent of the author.
 */
public class MinApplet extends Applet {

    /**
     * @throws HeadlessException
     */
    public MinApplet() throws HeadlessException {

    }

    public void init() {
	this.setBackground(Color.yellow);
    }

    public void paint(Graphics g) {
	g.setFont(new Font("Arial", Font.BOLD, 15));
	g.setColor(Color.red);
	g.drawString("Min applet!", 50, 70);
	g.drawString("V�lkommen till JAVA", 16, 90);
	g.setColor(Color.blue);
	g.drawOval(10, 10, 150, 150);
	g.setColor(new Color(255, 100, 0));
	g.fillOval(67, 105, 40, 40);
    }

}
