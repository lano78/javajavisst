package kap05;

import java.awt.Color;
import java.awt.Graphics;
import javax.swing.JFrame;
import javax.swing.JPanel;
/**
 * @author Lars Nordstr�m
 * @website http://www.lenbitz.se
 * @repo lano78@bitbucket.org
 * @twitter lano78@twitter
 * @date 6 aug. 2015
 * @license May not be redistributed or used without the consent of the author.
 */
import helpers.ColorRandomizer;

public class StapelDiagram extends JPanel {

    int[] data = { 90, 24, 12, 84, 40, 19, 54 };

    public StapelDiagram() {
	double max = 100;

    }

    @Override
    public void paintComponent(Graphics graphics) {
	super.paintComponent(graphics);
	int centerX = this.getWidth() / 2;
	int centerY = this.getHeight() / 2;

	Color color = new Color(getRandomRGB(), getRandomRGB(), getRandomRGB());

	graphics.setColor(color);
	drawDiagram(data, graphics);

    }

    public static void main(String[] args) {
	JFrame frame = new JFrame("Diagram");
	frame.setSize(400, 400);
	frame.setLocationRelativeTo(null);

	frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

	frame.add(new StapelDiagram());
	frame.setVisible(true);
    }

    public void drawDiagram(int[] d, Graphics graphics) {

	for (int i = 0; i < d.length; i++) {

	    Color color = ColorRandomizer.getRandomColor();
	    
	    if (i == 0) {
		graphics.fill3DRect(10, 300 - d[i], 25, d[i], true);
		graphics.drawString(Integer.toString(d[i]), 10, 300 - d[i]);

	    } else {
		graphics.fill3DRect(10 + (i * 25), 300 - d[i], 25, d[i], true);
		graphics.drawString(Integer.toString(d[i]), 13 + (i * 25), 300 - d[i]);

	    }
	    if (i % 2 == 0) {
		graphics.setColor(color);
	    } else {
		graphics.setColor(color);
	    }

	}
    }

    public int getRandomRGB() {
	return (int) (Math.random() * 255);
    }
    


}
