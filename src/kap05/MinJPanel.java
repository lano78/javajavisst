package kap05;

import java.awt.Color;
import java.awt.Graphics;
import java.beans.ConstructorProperties;

import javax.swing.JFrame;
import javax.swing.JPanel;
/**
 * @author Lars Nordstr�m
 * @website http://www.lenbitz.se
 * @repo lano78@bitbucket.org
 * @twitter lano78@twitter
 * @date 6 aug. 2015
 * @license May not be redistributed or used without the consent of the author.
 */
public class MinJPanel extends JPanel {
    
    @ConstructorProperties({"red", "green", "blue"})
    public MinJPanel(int red, int green, int blue) {
	this.setBackground(new Color(red, green, blue));
    }

    public void paintComponent(Graphics graphics) {
	super.paintComponent(graphics);
	int centerX = this.getWidth() / 2;
	int centerY = this.getHeight() / 2;
	graphics.drawRect(centerX, centerY, 100, 50);
	graphics.drawString("Text p� panelytan", centerX, centerY);
    }

    public static void main(String[] args) {
	
	JFrame frame = new JFrame("MinJPanel");
	frame.setSize(400, 400);
	frame.setLocationRelativeTo(null);

	frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

	frame.add(new MinJPanel(255, 225,233));
	frame.setVisible(true);
    }
    
}
