package kap01;

/**
 * @author Lars Nordstr�m
 * @website http://www.lenbitz.se
 * @repo lano78@bitbucket.org
 * @twitter lano78@twitter
 * @date 29 june. 2015
 * @license May not be redistributed or used without the consent of the author.
 */
public class Summera {

    public static void main(String[] args) {
	int x, y, sum;

	x = 17;
	y = 23;

	sum = x + y;

	System.out.println("Summan �r " + sum);

    }

}
