package kap01;

import java.util.Scanner;

/**
 * @author Lars Nordstr�m
 * @website http://www.lenbitz.se
 * @repo lano78@bitbucket.org
 * @twitter lano78@twitter
 * @date 29 june. 2015
 * @license May not be redistributed or used without the consent of the author.
 */
public class Summera2 {

    public static void main(String[] args) {

	Scanner sc = new Scanner(System.in);

	int x, y, sum;

	System.out.print("Skriv ett tal: ");
	x = sc.nextInt();

	System.out.print("Skriv ett tal till: ");
	y = sc.nextInt();

	sum = x + y;

	System.out.println("Summan �r " + sum);
    }

}
