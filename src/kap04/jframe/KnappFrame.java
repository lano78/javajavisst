/**
 * 
 */
package kap04.jframe;

import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;

/**
 * @author Lars Nordstr�m
 * @website http://www.lenbitz.se
 * @repo lano78@bitbucket.org
 * @twitter lano78@twitter
 * @date 29 june. 2015
 * @license May not be redistributed or used without the consent of the author.
 */

@SuppressWarnings("serial")
public class KnappFrame extends JFrame implements ActionListener {

    JLabel lbl;
    JButton btn1, btn2;

    public KnappFrame() {
	this.setLayout(new FlowLayout());
	lbl = new JLabel("Detta �r en JLabel!");
	this.add(lbl);
	lbl.setFont(new Font("Arial", Font.ITALIC, 20));
	lbl.setForeground(Color.YELLOW);
	lbl.setOpaque(true);
	lbl.setBackground(Color.blue);

	btn1 = new JButton("Bl�");
	btn1.addActionListener(this);
	this.add(btn1);
	btn2 = new JButton("R�d");
	btn2.addActionListener(this);
	this.add(btn2);

    }

    public static void main(String[] args) {

	KnappFrame f = new KnappFrame();

	f.setSize(220, 200);
	f.setLocation(100, 100);
	f.setTitle("Min f�rsta GUI-applikation");
	f.setDefaultCloseOperation(KnappFrame.EXIT_ON_CLOSE);

	f.setVisible(true);
    }

    @Override
    public void actionPerformed(ActionEvent event) {
	if (event.getSource() == btn1) {

	    lbl.setBackground(Color.blue);

	} else if (event.getSource() == btn2) {

	    lbl.setBackground(Color.red);
	}

    }

}
