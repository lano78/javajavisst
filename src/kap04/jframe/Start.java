/**
 * 
 */
package kap04.jframe;

/**
 * @author Lars Nordstr�m
 * @website http://www.lenbitz.se
 * @repo lano78@bitbucket.org
 * @twitter lano78@twitter
 * @date 29 june. 2015
 * @license May not be redistributed or used without the consent of the author.
 */

public class Start {

    /**
     * @param args
     */
    public static void main(String[] args) {

	KnappFrame f = new KnappFrame();

	f.setSize(220, 200);
	f.setLocation(100, 100);
	f.setTitle("Min f�rsta GUI-applikation");
	f.setDefaultCloseOperation(KnappFrame.EXIT_ON_CLOSE);

	f.setVisible(true);
    }

}
