/**
 * 
 */
package kap04.inmata;

import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

/**
 * @author Lars Nordstr�m
 * @website http://www.lenbitz.se
 * @repo lano78@bitbucket.org
 * @twitter lano78@twitter
 * @date 29 june. 2015
 * @license May not be redistributed or used without the consent of the author.
 */
@SuppressWarnings("serial")
public class Inmata extends JFrame implements ActionListener {

    JLabel lbl1, lbl2;
    JButton btn1;
    JTextField tf;
    String namn;

    public Inmata() {
	this.setLayout(new FlowLayout());
	lbl1 = new JLabel("Skriv ditt namn:");
	this.add(lbl1);

	tf = new JTextField(15);
	tf.addActionListener(this);
	this.add(tf);

	btn1 = new JButton("Tryck h�r");
	btn1.addActionListener(this);
	this.add(btn1);

	lbl2 = new JLabel();
	this.add(lbl2);

    }

    public static void main(String[] args) {

	Inmata f = new Inmata();

	f.setSize(400, 150);
	f.setLocation(100, 100);
	f.setTitle("Inmata");
	f.setDefaultCloseOperation(Inmata.EXIT_ON_CLOSE);

	f.setVisible(true);
    }

    @Override
    public void actionPerformed(ActionEvent event) {

	if (event.getSource() == btn1) {

	    namn = tf.getText();
	    lbl2.setText("Hej " + namn);
	    System.out.println("Hej " + namn);

	} else if (event.getSource() == tf) {

	    namn = tf.getText();
	    lbl2.setText("Hej " + namn);
	    System.out.println("Hej " + namn);
	}

    }

}
