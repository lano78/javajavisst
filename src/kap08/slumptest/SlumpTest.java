/**
 * 
 */
package kap08.slumptest;

/**
 * @author Lars Nordstr�m
 * @website http://www.lenbitz.se
 * @repo lano78@bitbucket.org
 * @twitter lano78@twitter
 * @date 1 july. 2015
 * @license May not be redistributed or used without the consent of the author.
 */
public class SlumpTest {

    /**
     * @param args
     */
    public static void main(String[] args) {
	double slump;
	for (int i = 1; i <= 12; i++) {
	    slump = Math.random();
	    System.out.println("Slumptal nummer " + i + ":" + slump);
	}

    }

}
