/**
 * 
 */
package kap08.rottabell;

/**
 * @author Lars Nordstr�m
 * @website http://www.lenbitz.se
 * @repo lano78@bitbucket.org
 * @twitter lano78@twitter
 * @date 1 july. 2015
 * @license May not be redistributed or used without the consent of the author.
 */
public class RotTabell {

    public static void main(String[] args) {
	System.out.println("x \t Roten ur x");
	System.out.println("-----------------------------");
	for (int x = 1; x <= 10; x++) {
	    System.out.println(x + "\t" + Math.sqrt(x));
	}

    }

}
