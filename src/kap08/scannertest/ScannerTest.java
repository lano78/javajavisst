/**
 * 
 */
package kap08.scannertest;

import java.util.Scanner;

import javax.swing.JPanel;

/**
 * @author Lars Nordstr�m
 * @website http://www.lenbitz.se
 * @repo lano78@bitbucket.org
 * @twitter lano78@twitter
 * @date 1 july. 2015
 * @license May not be redistributed or used without the consent of the author.
 */
@SuppressWarnings("serial")
public class ScannerTest extends JPanel {

    /**
     * @param args
     */
    public static void main(String[] args) {

	Scanner sc = new Scanner(System.in);

	System.out.print("Vilken ort? ");
	String ort = sc.next();

	System.out.print("L�nsbokstav ? ");
	char bokst = sc.next().charAt(0);

	System.out.print("Antal inv�nare? ");
	int inv = sc.nextInt();

	System.out.print("Hur l�ngt? (ange avst�nd i mil) ");
	double avst = sc.nextDouble();

	System.out.println(ort + " ligger i " + bokst + "-l�n");
	System.out.println(ort + " har " + inv + " inv�nare");
	System.out.println("Avst�nd till " + ort + ": " + avst + " mil");

	sc.close();
    }

}
