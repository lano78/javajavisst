/**
 * 
 */
package kap08.rottabell2;

import java.awt.Font;
import java.awt.Graphics;

import javax.swing.JFrame;
import javax.swing.JPanel;

/**
 * @author Lars Nordstr�m
 * @website http://www.lenbitz.se
 * @repo lano78@bitbucket.org
 * @twitter lano78@twitter
 * @date 1 july. 2015
 * @license May not be redistributed or used without the consent of the author.
 */
@SuppressWarnings("serial")
public class RotTabell2 extends JPanel {

    public void paintComponent(Graphics g) {
	g.setFont(new Font("Courier", Font.PLAIN, 20));
	g.drawString("x", 100, 40);
	g.drawString("Roten ur x", 220, 40);
	g.drawLine(50, 50, 450, 50);

	for (int x = 1; x <= 10; x++) {
	    g.drawString(x + "", 100, 50 + 20 * x);
	    g.drawString(Math.sqrt(x) + "", 200, 50 + 20 * x);
	}
    }

    /**
     * @param args
     */
    public static void main(String[] args) {
	JFrame frame = new JFrame();
	frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	frame.setSize(500, 400);
	frame.setLocation(100, 100);
	frame.setTitle("RotTabell2");
	RotTabell2 rtb2 = new RotTabell2();
	frame.add(rtb2);
	frame.setVisible(true);

    }

}
