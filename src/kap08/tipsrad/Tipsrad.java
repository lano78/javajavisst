/**
 * 
 */
package kap08.tipsrad;

/**
 * @author Lars Nordstr�m
 * @website http://www.lenbitz.se
 * @repo lano78@bitbucket.org
 * @twitter lano78@twitter
 * @date 1 july. 2015
 * @license May not be redistributed or used without the consent of the author.
 */
public class Tipsrad {

    /**
     * @param args
     */
    public static void main(String[] args) {
	double slump;
	for (int i = 1; i <= 13; i++) {
	    slump = Math.random();
	    if (slump < 0.4) {
		System.out.println("1  ");
	    } else if (slump < 0.8) {
		System.out.println(" X ");
	    } else {
		System.out.println("  2");
	    }

	}

    }

}
