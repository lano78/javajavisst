/**
 * 
 */
package kap08.consoletest;

import java.io.Console;

/**
 * @author Lars Nordstr�m
 * @website http://www.lenbitz.se
 * @repo lano78@bitbucket.org
 * @twitter lano78@twitter
 * @date 1 july. 2015
 * @license May not be redistributed or used without the consent of the author.
 */
public class ConsoleTest {

    /**
     * @param args
     */
    public static void main(String[] args) {

	System.out.println("Funkar endast i consol-milj�!");
	Console co = System.console();

	System.out.println("Vad heter orten? ");
	String ort = co.readLine();

	System.out.println("Lansbokstav? ");
	char bokst = co.readLine().charAt(0);

	System.out.println("Antal inv�nare? ");
	int inv = Integer.parseInt(co.readLine());

	System.out.println("Avst�nd? ");
	double avst = Double.parseDouble(co.readLine());

	System.out.println(ort + " ligger i " + bokst + "-l�n");
	System.out.println(ort + " har " + inv + " inv�nare");
	System.out.println("Avst�nd till " + ort + ": " + avst + " mil");

    }

}
