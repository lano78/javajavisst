package kap02.boll;

/**
 * @author Lars Nordstr�m
 * @website http://www.lenbitz.se
 * @repo lano78@bitbucket.org
 * @twitter lano78@twitter
 * @date 29 june. 2015
 * @license May not be redistributed or used without the consent of the author.
 */
public class SkapaBoll {

    public static void main(String[] args) {

	// Referens till Boll deklareras.
	Boll a, b;

	// Referensvariabeln f�r 'a' initierar ett objekt av typen Boll med hj�lp av Konstruktor 1.
	a = new Boll();

	// Referensvariabeln f�r 'b' initierar ett objekt av typen Boll med hj�lp av Konstruktor 2.
	// Argumenten anges och bollen skapas d�refter.
	b = new Boll(20, "Gummi", "Svart");

	a.rulla(20);
	System.out.println("Volymen: " + a.volym());
	b.rulla(5);
	System.out.println("Volymen: " + b.volym());

    }

}
