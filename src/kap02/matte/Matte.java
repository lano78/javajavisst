/**
 * 
 */
package kap02.matte;

/**
 * @author Lars Nordstr�m
 * @website http://www.lenbitz.se
 * @repo lano78@bitbucket.org
 * @twitter lano78@twitter
 * @date 29 aug. 2015
 * @license May not be redistributed or used without the consent of the author.
 */
public class Matte {

    public static final double PI = 3.14159;

    public static int sqr(int x) {
	return x * x;
    }
}
