package kap11;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.*;
import javax.swing.JFrame;
import javax.swing.JPanel;
/**
 * @author Lars Nordstr�m
 * @website http://www.lenbitz.se
 * @repo lano78@bitbucket.org
 * @twitter lano78@twitter
 * @date 7 aug. 2015
 * @license May not be redistributed or used without the consent of the author.
 */
public class Rita extends JPanel implements MouseMotionListener {
    
    
    private int x = 0, y = 0;
    private Color color = Color.BLACK; 
    public Rita() {
	this.addMouseMotionListener(this);
    }

    @Override
    public void paintComponent(Graphics graphics) {

	graphics.setColor(color);

	graphics.fillOval(x, y, 5, 5);

    }

    public static void main(String[] args) {
	JFrame frame = new JFrame();

	// width, height
	frame.setSize(400, 400);

	// x, y
	frame.setLocationRelativeTo(null);
	frame.setTitle("MenyTest");
	frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	frame.add(new Rita());
	frame.setVisible(true);
    }

    @Override
    public void mouseDragged(MouseEvent event) {
	x = event.getX();
	y = event.getY();
	setColor(event);
	this.repaint();
    }

    @Override
    public void mouseMoved(MouseEvent event) {

    }
    
    public void setColor(MouseEvent event){
	if(event.isMetaDown()){
	    this.color = Color.RED;
	}else{
	    this.color = Color.GREEN;
	}
    }



}
