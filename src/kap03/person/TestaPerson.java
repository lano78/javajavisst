/**
 * 
 */
package kap03.person;

/**
 * @author Lars Nordstr�m
 * @website http://www.lenbitz.se
 * @repo lano78@bitbucket.org
 * @twitter lano78@twitter
 * @date 29 june. 2015
 * @license May not be redistributed or used without the consent of the author.
 */
public class TestaPerson {

    /**
     * @param args
     */
    public static void main(String[] args) {
	Person p = new Person("Karlsson", "Hjo");
	System.out.println(p);

	Anstalld a = new Anstalld("Svensson", "Trosa", 15000);
	System.out.println(a);

	Forsaljare f = new Forsaljare("Svensson", "Trosa", 15000, 5);
	System.out.println(f);

    }

}
