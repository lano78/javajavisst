/**
 * 
 */
package kap03.person;

/**
 * @author Lars Nordstr�m
 * @website http://www.lenbitz.se
 * @repo lano78@bitbucket.org
 * @twitter lano78@twitter
 * @date 29 june. 2015
 * @license May not be redistributed or used without the consent of the author.
 */
public class Anstalld extends Person {

    protected int lon;

    /**
     * 
     */
    public Anstalld() {

    }

    /**
     * @param namn
     * @param ort
     */
    public Anstalld(String namn, String ort, int lon) {
	super(namn, ort);
	this.lon = lon;

    }

    public String toString() {
	return "-- " + this.getClass().getSimpleName() + " --\nNamn: " + namn + "\nOrt: " + ort + "\nL�n: " + lon
		+ "\r";
    }

}
