/**
 * 
 */
package kap03.arv;

/**
 * @author Lars Nordstr�m
 * @website http://www.lenbitz.se
 * @repo lano78@bitbucket.org
 * @twitter lano78@twitter
 * @date 29 june. 2015
 * @license May not be redistributed or used without the consent of the author.
 */
public class Boll {
    // Instansvariabler
    protected int diameter;
    protected String material;
    protected String farg;

    /*
     * Konstruktor 1 Skapar en boll med diametern 10, material = Plast, f�rg = Gul.
     */
    public Boll() {
	this.diameter = 10;
	this.material = "Plast";
	this.farg = "Gul";
    }

    /*
     * Konstruktor 2 Skapar en boll med utefter de argument som passeras in till konstruktorn.
     */
    public Boll(int diameter, String material, String farg) {
	this.diameter = diameter;
	this.material = material;
	this.farg = farg;
    }

    // Metoder

    /**
     * Denna metod returnerar volymen av bollen genom att anv�nda v�rdet av instansvariabeln</br>
     * diameter som s�tts i Konstruktorn.
     * 
     * @return volym
     */
    public double volym() {
	double v;
	v = 3.14159 * diameter * diameter * diameter / 6;
	return v;
    }

    /*
     * Denna metod rullar bollen tills v�rdet 'i' �verstiger v�rdet n.
     */
    public void rulla(int n) {
	int i = 0;
	while (i < n) {
	    System.out.print("O");
	    i++;
	}
	System.out.println();
    }

    /*
     * Denna metod s�tter diametern f�r bollen.
     * 
     * @param d
     */
    public void setDiameter(int diameter) {
	this.diameter = diameter;
    }

    /*
     * Denna metod h�mtar v�rdet av bollens diameter.
     */
    public int getDiameter() {
	return diameter;
    }

    /*
     * Denna metod h�mtar v�rdet av bollens f�rg.
     */
    public String getFarg() {
	return farg;
    }

}
