/**
 * 
 */
package kap03.jframe;

/**
 * @author Lars Nordstr�m
 * @website http://www.lenbitz.se
 * @repo lano78@bitbucket.org
 * @twitter lano78@twitter
 * @date 29 june. 2015
 * @license May not be redistributed or used without the consent of the author.
 */

public class VisaJFrame {

    /**
     * @param args
     */
    public static void main(String[] args) {
	MinJFrame f;
	f = new MinJFrame();
	f.setSize(200, 200);
	f.setLocation(200, 300);
	f.setTitle("JFrame");
	f.setDefaultCloseOperation(MinJFrame.EXIT_ON_CLOSE);
	f.setVisible(true);
    }

}
