/**
 * 
 */
package kap03.kombibilar;

/**
 * @author Lars Nordstr�m
 * @website http://www.lenbitz.se
 * @repo lano78@bitbucket.org
 * @twitter lano78@twitter
 * @date 29 june. 2015
 * @license May not be redistributed or used without the consent of the author.
 */
public class KPersonbil {

    protected String marke;
    protected int arsmodell;
    protected int nypris;
    protected int storlek;

    /**
     * Konstruktor 1 skapar en ny bil med f�rdefinerade egenskaper</br>
     * . M�rke = Volvo, �rsmodell = 2000, Nypris = 200000, Storlek = 3.
     */
    public KPersonbil() {
	marke = "Volvo";
	arsmodell = 2000;
	nypris = 200000;
	storlek = 3;
    }

    /**
     * Konstruktor 2 skapar en bil utifr�n de argument som anges i konstruktorn.
     */
    public KPersonbil(String marke, int arsmodell, int nypris, int storlek) {
	this.marke = marke;
	this.arsmodell = arsmodell;
	this.nypris = nypris;
	this.storlek = storlek;
    }

    /**
     * Denna metod ger en retur av typen String.
     * 
     * @return String
     */
    public String fakta() {
	return marke + " " + arsmodell + "\nnypris " + nypris + "kr";
    }

    /**
     * Denna metod r�knar ut v�rdet p� personbil.
     * 
     * @param ar
     * @return
     */
    public int varde(int ar) {
	int nupris;
	nupris = nypris - 10000 * (ar - arsmodell);
	if (nupris < 10000)
	    nupris = 10000;
	return nupris;
    }

    /**
     * Skriver ut ett fordon
     */
    public void visa() {
	System.out.print("    ");
	int i = 0;
	while (i < storlek) {
	    System.out.print("_");
	    i++;
	}
	System.out.println();
	System.out.print(" __/");
	i = 0;
	while (i < storlek) {
	    System.out.print("_");
	    i++;
	}
	System.out.println("\\_");
	System.out.print(" -O-");
	i = 0;
	while (i < storlek) {
	    System.out.print("-");
	    i++;
	}
	System.out.println("O- ");
    }

}
