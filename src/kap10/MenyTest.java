package kap10;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;

import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;

/**
 * @author Lars Nordstr�m
 * @website http://www.lenbitz.se
 * @repo lano78@bitbucket.org
 * @twitter lano78@twitter
 * @date 7 aug. 2015
 * @license May not be redistributed or used without the consent of the author.
 */
public class MenyTest extends JFrame {

    public MenyTest() {

	// Initierar en MenuBar, en Container f�r alla menyer. JMenuBar �r subklass till JComponent.
	JMenuBar menu = new JMenuBar();

	// Initierar File menyn och dess sub-komponenter.
	JMenu fileMenu = new JMenu();

	// Meny title.
	fileMenu.setText("File");

	// Kort kommando, dvs alt + f.
	fileMenu.setMnemonic(KeyEvent.VK_F);

	// Skapar nya instanser f�r varje objekt.
	JMenuItem fileNew = new JMenuItem("New");
	JMenuItem fileOpen = new JMenuItem("Open");
	JMenuItem fileExit = new JMenuItem("Exit");

	// Tilldelar en ActionListener f�r varje meny alternativ f�r att g�ra dom klickbara.
	fileMenu.add(fileNew).addActionListener(new MenuActionListener());
	fileMenu.add(fileOpen).addActionListener(new MenuActionListener());
	fileMenu.add(fileExit).addActionListener(new MenuActionListener());

	// L�gger till File menyn till menuBar.
	menu.add(fileMenu);

	// Initierar Edit menyn och dess sub-komponenter.
	JMenu editMenu = new JMenu("Edit");

	// Kort kommando, dvs alt + e
	editMenu.setMnemonic(KeyEvent.VK_E);
	editMenu.add(new JMenuItem("Cut")).addActionListener(new MenuActionListener());
	editMenu.add(new JMenuItem("Copy")).addActionListener(new MenuActionListener());
	editMenu.add(new JMenuItem("Paste")).addActionListener(new MenuActionListener());
	menu.add(editMenu);

	this.setJMenuBar(menu);

	this.setSize(400, 400);

	this.setLocationRelativeTo(null);
	this.setTitle("MenyTest");
	this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	this.setVisible(true);
    }

    public static void main(String[] args) {

	new MenyTest();

    }

    /**
     * Privat innerklass f�r hantering av klick i menyer.
     * 
     * @author Lars Nordstr�m
     * @website http://www.lenbitz.se
     * @repo lano78@bitbucket.org
     * @twitter lano78@twitter
     * @date 7 aug. 2015
     * @license May not be redistributed or used without the consent of the author.
     */
    private static final class MenuActionListener implements ActionListener {
	public void actionPerformed(ActionEvent event) {
	    // Klickar man p� Exit i File menyn avslutas programmet.
	    if (event.getActionCommand() == "Exit") {
		System.out.println(event.getActionCommand());
		// H�r kan man ev. spara applicationstate

		System.exit(0);
	    } else {
		System.out.println(event.getActionCommand());
	    }

	}
    }

}
